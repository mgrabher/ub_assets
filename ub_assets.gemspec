# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

require 'date'

Gem::Specification.new do |s|
  s.name = "ub_assets"
  s.version = "0.0.7"

  s.authors = ["Marcel Grabher"]
  s.date = Date.today
  s.description = "reusable assets for carrierwave"
  s.summary = "reusable assets for carrierwave"
  s.email = ["grabher.fun@gmail.com"]
  s.extra_rdoc_files = ["README.md"]
  s.files = Dir.glob("{bin,lib}/**/*") + %w(README.md)
  s.homepage = %q{https://bitbucket.org/mgrabher/ub_assets}
  s.rdoc_options = ["--main"]
  s.require_paths = ["lib"]
  s.rubyforge_project = %q{ub_assets}
  s.rubygems_version = %q{1.3.5}
  s.specification_version = 3
  s.licenses = ["MIT"]

  s.add_dependency 'rails', '>= 3'
  s.add_development_dependency "rspec", "~> 2.13.0"

end

