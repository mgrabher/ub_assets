![Usebased image](http://usebased.com/usebased.png)

# ub_assets * early alpha *

### Description

It s and extension for Active Record and Carrierwave.
It allows to make assets like videos, images to be reusable via references.
You don't need to do changes in your controllers to use it.

##### DEMOApp

https://github.com/banditj/carrierwave_plupload_demo

#### Install
1) gem


```
gem 'ub_assets'
```
2) migration 1:

```
class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :name
      t.string :file
      t.string :type

      t.timestamps
    end
  end
end
```
3) migration 2:

```
class CreateAssetLinks < ActiveRecord::Migration
  def change
    create_table :asset_links do |t|
      t.integer :asset_id
      t.integer :linker_id
      t.string :linker_type
      t.string :linker_attr

      t.timestamps
    end
  end
end
```
4) create Asset classes and uploaders

```
class Image < Asset

  mount_uploader :file , ImageUploader

end
```

```
class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :mittel do
    process :resize_to_fill => [100, 100]
  end

  version :thumb do
    process :resize_to_fill => [30, 30]
  end

end
```
5) Includes for your models

```
class Page < ActiveRecord::Base
  include Assetable

  has_assets :gallery_images  , type: Image
  has_asset :background_image  , type: Image

end

```

6.1) View with file fields

```
  .field
    = f.label :gallery_images
    = f.file_field :gallery_images, multiple: true
    
  .field
    = f.label :background_image
    = f.file_field :background_image

```

6.2) View with hidden fields for javascript functions to fill with ids of the assets (see the demo app / books)

```
  .field
    = f.label :gallery_images
    = f.hidden_field :gallery_images            # accepts multiple ids like 12,4,45,6

  .field
    = f.label :background_image
    = f.hidden_field :background_image          # accepts one id like 12

```

7) done

### License
MIT with reference to usebased.com

