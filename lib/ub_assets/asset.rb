
class Asset < ActiveRecord::Base

  has_many :asset_links

  # shows the classes that inherit from Asset = your Types of Assets
  @subclasses = []
  class << self
    attr_accessor :subclasses
  end
  def self.inherited(subclass)
    @subclasses = @subclasses | [subclass]
    super(subclass)
  end

  # searches for links to assets in string
  # convention: all assets have to be in /public/uploads
  # with the class name pluralized and underscored
  # like this
  # example /uploads/pdfs/1/x.pdf
  # returns Array [[1, "Pdf"], [2, "Image"]]
  def self.assets_in_string(str, ids_only=false)
    defined_assets = {}
    Asset.subclasses.each { |klass| defined_assets[klass.name.to_s.pluralize.underscore]= klass.name.to_s}
    allowed_names_for_regex = defined_assets.keys.join('|')
    links = str.scan(/(\/uploads\/(#{allowed_names_for_regex})\/[0-9]*)/) # improvable
    links = links | links # dublikate entfernen
    assets = [];asset_ids = []
    links.each do |link|
      link_arr=link[0].split('/')
      plural_name = defined_assets[link_arr[2]]
      id = link_arr[3].to_i
      assets << [id, plural_name]
      asset_ids << id
    end
    return ids_only ? asset_ids: assets
  end

  def update_asset_links

  end


  def link(linker, linker_attr)
    asset_links.create(asset:self, linker: linker, linker_attr:linker_attr)
  end

  def unlink(linker, linker_attr)
    asset_links.where(linker: linker, linker_attr: linker_attr)
  end

  attr_accessor :destroyed
  after_destroy :mark_as_destroyed
  def mark_as_destroyed
    self.destroyed = true
  end

end

class AssetTypes
  @subclasses = []

  class << self
    attr_accessor :subclasses
  end
end

