module Assetable

  extend ActiveSupport::Concern

  included do

    scope :with_assets, -> { includes(:asset_links) }

    # Include relations for :through follow-ups
    has_many :asset_links, as: :linker, :dependent => :destroy
    has_one :asset_link, as: :linker, :dependent => :destroy

    has_many :assets, through: :asset_links

    # Validation stuff like this because i don't want
    # to check for class types 2 times and for every potential asset binding
    attr_accessor :ub_assets_validation_errors
    after_initialize :init

    def init
      self.ub_assets_validation_errors = []
    end

    validate :validate_ub_assets

    def validate_ub_assets
      return true if @ub_assets_validation_errors.length == 0
      @ub_assets_validation_errors.each do |e|
        errors.add e[0], e[1]
      end
      false
    end

    before_save :sync_asset_links_in_strings
    def sync_asset_links_in_strings
      self.class.assetable_string_attributes.each do |attr|
        assets_in_string = Asset.assets_in_string(send(attr))
        unless assets_in_string.empty?
          assets_in_db = ActiveRecord::Base.connection.select_values("SELECT asset_id FROM asset_links WHERE linker_id='#{self.id}' AND linker_type='#{self.class.name.to_s}' AND linker_attr='#{attr.to_s}'")
          asset_ids_in_string = assets_in_string.map { |n| n[0] }
          assets_to_add = asset_ids_in_string - assets_in_db
          assets_to_delete = assets_in_db - asset_ids_in_string
          asset_validation_query = ""
          asset_in_string_counter = 0
          assets_in_string_num = assets_in_string.length
          assets_in_string.each do |asset_arr|
            asset_in_string_counter = asset_in_string_counter + 1
            asset_validation_query << "(id='#{asset_arr[0]}' AND type='#{asset_arr[1]}' )"
            asset_validation_query << " OR " unless asset_in_string_counter == assets_in_string_num
          end
          valid_asset_ids = ActiveRecord::Base.connection.select_values("SELECT id FROM assets WHERE " + asset_validation_query)
          if assets_in_string_num == valid_asset_ids.length
            self.asset_links.where(['linker_attr = ? AND asset_id in (?)', attr.to_s, assets_to_delete]).destroy_all unless assets_to_delete.empty?
            assets_to_add.each do |asset_id|
              self.asset_links.build(:linker_attr => attr.to_s, :asset_id => asset_id)
            end unless assets_to_add.empty?
          else
            assets_not_found = []
            assets_in_string.each do |asset_arr|
              assets_not_found << asset_arr unless valid_asset_ids.include? asset_arr[0]
            end
            unless assets_not_found.empty?
              assets_not_found.each do |asset|
                self.ub_assets_validation_errors.push([:base, attr.to_s + ': ' + asset[1].pluralize.underscore + '::' + asset[0].to_s + " is invalid"])
                self.ub_assets_validation_errors.push([attr, "/uploads/" + asset[1].pluralize.underscore + '/' + asset[0].to_s + " is not a valid Asset-Path"])
              end
              self.valid?
              return false
            end
          end
        end
      end
    end

    after_save :reload_asset_links

    def reload_asset_links
      unless self.class.assetable_string_attributes.empty?
        self.asset_links.reload
      end
    end
  end

  module ClassMethods

    # get attributes
    def assetable_attributes=(attr)
      @assetable_attrs ||= []
      @assetable_attrs << attr
    end

    def assetable_attributes
      @assetable_attrs ||= []
      @assetable_attrs
    end

    # define string attributes of the model that could contain links to assets
    def has_asset_links_in(string_attribute)
      @containing_attrs ||= []
      @containing_attrs << string_attribute
    end

    def assetable_string_attributes
      @containing_attrs ||= []
      @containing_attrs
    end


    def has_assets(plural_name, *options)
      self.assetable_attributes = (plural_name)
      has_many plural_name, -> { where "linker_attr = '#{plural_name.to_s}'" }, through: :asset_links, :source => :asset

      # set method for receiving array with assets
      # assets=[array]
      define_method(plural_name.to_s+"=") { |assets|
        #delete via standard functionality from active record
        super(assets) if assets == nil

        # ARRAY ###########
        if assets.class == Array || assets.class == ActiveRecord::Relation::ActiveRecord_Relation_Asset
          assets.each do |asset|
            if asset.is_a? ActionDispatch::Http::UploadedFile
              asset = Asset.new(type: options[0][:type].to_s, file: asset)
            elsif asset.is_a? options[0][:type]
              # ok
            else
              self.ub_assets_validation_errors.push([:base, plural_name.to_s + " is invalid"])
              self.ub_assets_validation_errors.push([plural_name.to_s, "has to be Asset or ActionDispatch::Http::UploadedFile. You tried:" + singular_name.to_s])
              return false
            end
            asset_links.build(asset: asset, linker: self, linker_attr: plural_name.to_s)
          end
          return true

          # Accept Target Object too, for adding ###########
        elsif  assets.class == options[0][:type]
          #ok
          asset_links.build(asset: assets, linker: self, linker_attr: plural_name.to_s)
          # String with Ids , ###########
        elsif  assets.class == String

          return false if assets=="" #if form passes empty string do nothing  /^[0-9]{1,}([,.][0-9]{1,})*$/
          if assets =~ /^[0-9]{1,}([,.][0-9]{1,})*$/

            assets_objs = options[0][:type].find(assets.split(","))
            assets_objs.each do |asset|
              asset_links.build(asset: asset, linker: self, linker_attr: plural_name.to_s)
            end
            return true
          else
            self.ub_assets_validation_errors.push([:base, plural_name.to_s + " is invalid"])
            self.ub_assets_validation_errors.push([plural_name.to_s, "illegal string format , try: 1,2,3"])
            return false
          end
        else
          self.ub_assets_validation_errors.push([:base, plural_name.to_s + " is invalid"])
          self.ub_assets_validation_errors.push([plural_name.to_s, "wrong object type: try Array with Assets or a commasparated String with Ids"])
          return false
        end
      }

      # unlink_asset(asset)
      define_method("unlink_"+plural_name.to_s.singularize) { |asset|
        asset_links.where(linker_attr: plural_name.to_s, asset: asset).delete_all
      }
    end

    def has_asset(singular_name, *options)
      self.assetable_attributes = (singular_name)

      has_one singular_name, -> { where "linker_attr = '#{singular_name.to_s}'" }, through: :asset_link, :source => :asset

      define_method("unlink_"+singular_name.to_s) { |asset|
        asset_links.where(linker_attr: singular_name.to_s, asset: asset).delete_all
      }

      define_method(singular_name.to_s+"=") { |asset|
        #delete via standard functionality   from active record
        super(asset) if asset == nil

        if asset.is_a? options[0][:type]
          # ok
        elsif asset.class == ActionDispatch::Http::UploadedFile
          asset = Asset.new(type: options[0][:type].to_s, file: asset)
        elsif asset.class == String
          return false if asset=="" #if form passes empty string do nothing  /^[0-9]{1,}([,.][0-9]{1,})*$/
          if asset =~ /^[0-9]{1,}([,.][0-9]{1,})*$/
            ids_arr = asset.split(",")
            if ids_arr.length == 1
              asset = options[0][:type].find(ids_arr[0])
            else
              self.ub_assets_validation_errors.push([:base, singular_name.to_s + " is invalid"])
              self.ub_assets_validation_errors.push([singular_name.to_s, "you can only assign one asset to " + singular_name.to_s])
              return false
            end
          else
            self.ub_assets_validation_errors.push([:base, singular_name.to_s + " is invalid"])
            self.ub_assets_validation_errors.push([singular_name.to_s, "illegal string> has to be numeric"])
            return false
          end
        else # no string, no uploaded file, no asset, dafug?
          self.ub_assets_validation_errors.push([:base, singular_name.to_s + " is invalid"])
          self.ub_assets_validation_errors.push([singular_name.to_s, "illegal type : " + asset.class.to_s])
          return false
        end
        # asset now is an asset - add reference
        asset_links.where(linker_attr: singular_name.to_s).delete_all
        asset_links.build(linker_attr: singular_name.to_s, asset: asset, linker: self)
      }

    end

  end

end

