require "spec_helper"


describe Asset do

  before (:each) do

    class Pdf < Asset; end
    class Image < Asset; end
    class Video < Asset; end


    # A page that includes 2 of the same images in img-tags, a href to a pdf and a video
    @page = '<div class="row-fluid bottom-space">
              <div id="template-header" class="span12 span-back">
              <p><img src="../../uploads/images/26/1.2d.png" alt="" width="454" height="341" /><span style="font-size: 12pt;">&nbsp;</span></p>
              </div>
              </div>
              <div class="row-fluid bottom-space">
              <div id="content1" class="span6 span-back">
              <p><a href="../../uploads/pdfs/234/1.2d.png" alt="" width="454" height="341" /></p>
              <p>&nbsp;</p>
              <p><strong>Ein gemeinsames Weltbild.</strong></p>
              </div>
              <div id="content2" class="span6 span-back"><img src="../../uploads/images/26/1.2d.png" alt="" width="454" height="341" /></div>
              </div>
              <div class="row-fluid bottom-space">
              <div id="template-footer" class="span12 span-back">
              <p>&nbsp;<img src="../../uploads/videos/26/1.2d.png" alt="" width="454" height="341" /></p>
              </div>
              </div>'

    @page_undefined_asset = '<div class="row-fluid bottom-space">
              <div id="template-header" class="span12 span-back">
              <p><img src="../../uploads/images/26/1.2d.png" alt="" width="454" height="341" /><span style="font-size: 12pt;">&nbsp;</span></p>
              </div>
              </div>
              <div class="row-fluid bottom-space">
              <div id="content1" class="span6 span-back">
              <p><a href="../../uploads/pdfs/234/1.2d.png" alt="" width="454" height="341" /></p>
              <p>&nbsp;</p>
              <p><strong>Ein gemeinsames Weltbild.</strong></p>
              </div>
              <div id="content2" class="span6 span-back"><img src="../../uploads/images/26/1.2d.png" alt="" width="454" height="341" /></div>
              </div>
              <div class="row-fluid bottom-space">
              <div id="template-footer" class="span12 span-back">
              <p>&nbsp;
<img src="../../uploads/videos/26/1.2d.png" alt="" width="454" height="341" />
<img src="../../uploads/HIUGO/26/1.2d.png" alt="" width="454" height="341" /></p>
              </div>
              </div>'

    @page_more_then_expected = '<div class="row-fluid bottom-space">
              <div id="template-header" class="span12 span-back">
              <p><img src="../../uploads/images/26/1.2d.png" alt="" width="454" height="341" /><span style="font-size: 12pt;">&nbsp;</span></p>
              </div>
              </div>
              <div class="row-fluid bottom-space">
              <div id="content1" class="span6 span-back">
              <p><a href="../../uploads/pdfs/234/1.2d.png" alt="" width="454" height="341" /></p>
              <p>&nbsp;</p>
              <p><strong>Ein gemeinsames Weltbild.</strong></p>
              </div>
              <div id="content2" class="span6 span-back"><img src="../../uploads/images/26/1.2d.png" alt="" width="454" height="341" /></div>
              </div>
              <div class="row-fluid bottom-space">
              <div id="template-footer" class="span12 span-back">
              <p>&nbsp;
<img src="../../uploads/videos/26/1.2d.png" alt="" width="454" height="341" />
<img src="../../uploads/images/45/1.2d.png" alt="" width="454" height="341" /></p>
              </div>
              </div>'

    @expectation = [[26, "Image"], [234, "Pdf"], [26, "Video"]]
    @expected_ids = [26, 234, 26]

  end

  it "extract asset type and id from string with the right function" do
    Asset.assets_in_string(@page).should eq(@expectation)
  end

  it "extract asset type and id from string with the right function" do
    Asset.assets_in_string(@page_more_then_expected).should_not eq(@expectation)
  end

  it "should ignore undefined asset-links" do
    Asset.assets_in_string(@page_undefined_asset).should eq(@expectation)
  end

  it "extract ids from string with the right function" do
    Asset.assets_in_string(@page,true).should eq(@expected_ids)
  end

end

