### Todo
- caching json with assets in model
- testing unlink function
- write tests
- md5 hash uniqueness for files, onExist -> link

###Changes - ub_assets

####0.0.7
- has_assets attributes accepts Target Object without being included in an array for adding to the collection
- Asset.subclasses shows you your types of assets
- Asset.assets_in_string(string, ids_only=false) - returns all links to assets in the string as [34, Pdf]
        But there is a convention
          # searches for links to assets in string
          # convention: all assets have to be in /public/uploads
          # with the class name pluralized and underscored
          # like this
          # example /uploads/pdfs/1/x.pdf
          # returns Array [1, "Pdf"] or [1,2,4]
- String-Attributes now can contain asset-links like /uploads/images/1/...
  use this in your assetable model=> has_asset_links_in(string_attribute)
  CAUTION: keep in mind the convention for the carrierwave store_dir in your uploader
  it has to be: / + uploads + / + model.name.pluralize.underscore + / + model.id

####0.0.6

- asset_link and asset have a destroyed attribute after destroyed successfully

####0.0.5

- fix: asset=nil or assets=nil didn't delete the references

####0.0.4

- remove not needed file, caused error

####0.0.3

- has_asset attributes now accept String with an Integer as type for the = method. It automatically finds the Asset via the id and makes a new asset_assignment

- has_assets attributes accepts an String with comma speparated Integers for the same behavior

- added validationsmessages

####0.0.2

- fixes

####0.0.1          

- initial